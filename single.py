#!/usr/bin/env python

import numpy as np
from nnenc import *
import math
from scipy.stats import ttest_ind
from itertools import product
import pickle


# set a random numberinitialization seed to make code reproducible:
lasagne.random.set_rng(np.random.RandomState(seed=1)) # for lasagne
np.random.seed(seed=1) # for shuffling training examples

testnn = NNenc("data/HLAs.txt")
testnn.encode_data("data/HLAs.txt", save=True)
testnn.train(epochs=100000, n_hid=50, noise=0.2)
testnn.save_input_weights()
testnn.save_hidden_outputs()




