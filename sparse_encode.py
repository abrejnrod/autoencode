#!/usr/bin/env python

import data_io_func
import numpy as np

alphabet = "ARNDCQEGHILKMFPSTWYVX"

def sparse_encode(data):
    new_data = np.zeros((len(data),len(data[0]), 21)) + 0.05
    for pepi,peptide in enumerate(data):
        for peppos, pepaa in enumerate(peptide):
            for aai,letter in enumerate(alphabet):
                if pepaa == letter:
                    new_data[pepi, peppos, aai] = 0.9
    return(new_data)
