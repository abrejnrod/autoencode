#!/usr/bin/env python

import numpy as np
from nnenc import *
import math
from scipy.stats import ttest_ind
from itertools import product
import pickle


# set a random numberinitialization seed to make code reproducible:
lasagne.random.set_rng(np.random.RandomState(seed=1)) # for lasagne
np.random.seed(seed=1) # for shuffling training examples

#set up grid
hidden_nodes = range(1,189, 20)+[189]
batch_sizes = [1,20,100,500, 1050]
epochs = [200, 1000, 10000]

grid = list(product(hidden_nodes, batch_sizes, epochs))
print(len(grid))
results = []
for index,it in enumerate(grid):
    testnn = NNenc("data/HLAs.txt")
    testnn.train(epochs = it[2], batchsize=it[1], n_hid=it[0])
    results.append(testnn.hweights)
    print index
with open('results.pickle', 'wb') as outfile:
    pickle.dump( results, outfile )
#print np.var(testnn.hweights, axis=0)
#for dim1 in range(testnn.hweights.shape[1]):
#    print(ttest_ind(testnn.hweights[1:525,dim1], testnn.hweights[525:1050,dim1])[0])
#testnn.plot_hidden_weights(pd.Series([1]*525 + [2]*525, name="HLA"))
#print testnn.hweights.shape


