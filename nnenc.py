#!/usr/bin/python
from __future__ import print_function
import time
import numpy as np
import theano
import theano.tensor as T
import lasagne
import pandas as pd
from ggplot import *
import data_io_func
from sparse_encode import *

class NNenc:
    def __init__(self, data):
        self.data = data
        self.pep_len = 0
        self.data_enc = self.encode_data(data)
        

    def encode_data(self, datafile, save=False):
        # read in peptide sequences and targets:
        data = [x.strip() for x in open(datafile, "r").readlines()]
        self.pep_len = len(max(data))
        # encode data using BLOSUM50:
        data_enc = sparse_encode(data)
        #transform to x, 9*21
        n_peptides = len(data)
        data_enc = data_enc.reshape((n_peptides, self.pep_len*21))
        if save:
            np.savetxt("blenc.csv", data_enc, delimiter=",")
        return(data_enc)

    def build_NN(self, n_features, n_hid):
        # input layer:
        l_in= lasagne.layers.InputLayer((None,self.pep_len*n_features))
        # add hidden layer:
        l_hid = lasagne.layers.DenseLayer(
        l_in,
        num_units=n_hid,
        nonlinearity=lasagne.nonlinearities.sigmoid,
        W=lasagne.init.Normal(),
            name='hidden',b=None)

        # output layer:
        l_out = lasagne.layers.DenseLayer(
            l_hid,
            num_units=n_features*self.pep_len,
            nonlinearity=lasagne.nonlinearities.sigmoid,
            W=lasagne.init.Normal(), b=None)
        return l_out,l_in, l_hid

    def build_NN_noisy(self, n_features, n_hid, noise=0.2):
        # input layer:
        l_in= lasagne.layers.InputLayer((None,self.pep_len*n_features))
        l_drop = lasagne.layers.DropoutLayer(l_in, p = noise)
        # add hidden layer:
        l_hid = lasagne.layers.DenseLayer(
        l_drop,
        num_units=n_hid,
        nonlinearity=lasagne.nonlinearities.sigmoid,
        W=lasagne.init.Normal(),
            name='hidden',b=None)
        # output layer:
        l_out = lasagne.layers.DenseLayer(
            l_hid,
            num_units=n_features*self.pep_len,
            nonlinearity=lasagne.nonlinearities.sigmoid,
            W=lasagne.init.Normal(), b=None)
        return l_out,l_in, l_hid
    
    def train(self, epochs=200, batchsize=20, n_hid=10, noise=False):
        data_enc = self.data_enc
        if noise == False:
            network,inp, hidden = self.build_NN(n_features=21, n_hid=n_hid) #Set to autoencoder dimensionality
        else:
            network,inp, hidden = self.build_NN_noisy(n_features=21, n_hid=n_hid, noise=noise) #Set to autoencoder dimensionality
        sym_target = T.matrix('targets',dtype='float32')
        sym_l_rate=T.scalar()
        prediction = lasagne.layers.get_output(network)
        loss = lasagne.objectives.squared_error(prediction.flatten(), sym_target.flatten())
        loss = loss.mean()
        params = lasagne.layers.get_all_params(network, trainable=True)
        updates = lasagne.updates.sgd(loss, params, learning_rate=sym_l_rate)
        train_fn = theano.function([inp.input_var, sym_target, sym_l_rate], loss, updates=updates, allow_input_downcast=True)
        pred_hidden = lasagne.layers.get_output(hidden)
        pred_input = lasagne.layers.get_output(inp)
        param_input = lasagne.layers.get_all_param_values(hidden)
        output_fn = theano.function([inp.input_var], pred_hidden)
        output_input = theano.function([inp.input_var], pred_input)


        EPOCHS=range(1,epochs)
        LEARNING_RATE=0.01
        print("# Start training loop...")
        start_time = time.time()
        x = T.matrix('x')
        b_epoch=0
        for e in EPOCHS:
            train_err = 0
            train_batches = 0
            e_start_time = time.time()

            for batch in self.iterate_minibatches(data_enc, data_enc, batchsize):
                Xinp, target = batch
                train_err += train_fn(Xinp, target, LEARNING_RATE)
                train_batches += 1
            if e % 50 == 0:
                print(e, train_err/train_batches)
        hweights = output_fn(data_enc)
        self.hweights = hweights
        self.iparams = lasagne.layers.get_all_param_values(network)[0]
        self.save_hidden_outputs(filename = "hidden_outputs" + str(n_hid) + str(batchsize) + str(epochs) + str(noise) + ".csv")
        self.save_input_weights(filename = "weights" + str(n_hid) + str(batchsize) + str(epochs) + str(noise) + ".csv")
        
    def iterate_minibatches(self,pep, targets, batchsize):
        assert pep.shape[0] == targets.shape[0]
        # shuffle:
        indices = np.arange(len(pep))
        np.random.shuffle(indices)
        for start_idx in range(0, len(pep) - batchsize + 1, batchsize):
            excerpt = indices[start_idx:start_idx + batchsize]
            yield pep[excerpt],targets[excerpt]

    def plot_hidden_weights(self, labels, labelname ='HLA'):
        hweights_df = pd.DataFrame(self.hweights, columns=map(str,range(self.hweights.shape[1])))
        hweights_df[labelname] = labels#pd.Series([1]*10 + [2]*10, name="HLA") #TODO: set this back to labels
        hweights_dfm = pd.melt(hweights_df, id_vars=['HLA'])
        p = ggplot(hweights_dfm, aes(x = 'HLA', y = 'value'))+geom_boxplot()  + facet_wrap('variable') 
        print(p)

    def plot_scatter(self, labels, neuron1=1, neuron2=2, labelname='HLA'):
        hweights_df = pd.DataFrame(self.hweights, columns=map(str,range(self.hweights.shape[1])))
	hweeights_df[labelname] = labels#pd.Series([1]*10 + [2]*10, name="HLA")
        p = ggplot(hweights_df, aes(x=str(neuron1), y=str(neuron2), color=labelname)) + geom_point()
        print(p)

    def save_hidden_outputs(self, filename = "hidden_outputs.csv" ):
        np.savetxt(filename, self.hweights, delimiter=",")
        
    def save_input_weights(self,filename = "input_weights.csv" ):
        np.savetxt(filename, self.iparams, delimiter=",")
